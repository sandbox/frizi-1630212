<?php

function extract_content($he_url, $tag, $he_id){

  $curl_connection = curl_init();
  curl_setopt($curl_connection, CURLOPT_URL, $he_url);
  curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
  curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
  curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
  $data = curl_exec ($curl_connection);
  curl_close ($curl_connection);

  $page_content = qp($data, 'body')->find('div#content')->first();

   if($page_content->size() == NULL){
     rules_invoke_event('html_extract_page_missing', $he_id . '-' . $he_url . ' ->  not found');
     return(FALSE);
   }

  
  // size = count - 1 because we start from 0
  $size = $page_content->children()->size() - 1;

  $temp_arr = array();
  for($i=0; $i<= $size; $i++){

    // filter extended special ascii characters
    if(clean_tag($page_content->html())){
     $temp_arr[] = array(
         'he_id' => $he_id,
         'he_tag' => $page_content->tag(),
         'he_content' => $page_content->html()
        );
    }
    $page_content->next();
  }
  return($temp_arr);
}


function clean_tag($he_string){
  $filtered_value = preg_replace('/[^(\x20-\x7F)]*/','', trim(strip_tags($he_string)));
  $filtered_value = str_replace(array('<br/>', '<br />'), "\n", trim($filtered_value));

  if(!empty($filtered_value)){
    return($filtered_value);
  }
  else{
    return(FALSE);
  }
}

function update_html_content($actual_content, $he_id, $he_update_flag = 1) {

  $the_flag = FALSE;
  $filtered_actual_content = array();

  try {

    // get the lastest date which will considered as refference
    $max_time_query = db_select('html_extract_content', 'hec');
    $max_time_query -> fields('hec', array('he_updated_date'))
           -> addExpression('MAX(hec.he_updated_date)');
    $max_time = $max_time_query-> execute()
                               -> fetchAll();

    $max_time = (isset($max_time[0]->he_updated_date) && is_int((int)$max_time[0]->he_updated_date)) ? $max_time[0]->he_updated_date : 0;

    $query = db_select('html_extract_content', 'hec');

    $query -> fields('hec')
    -> condition('hec.he_id', $he_id, '=')
    -> condition('hec.he_updated_date', $max_time, '=')
    -> orderBy('he_content_id', 'ASC');

    $query_result = $query-> execute()
                          -> fetchAll();

  }
  catch (Exception $e) {
    return(FALSE);
  }

  try {
    $query = db_select('html_extract_content', 'hec');
    $query_result = $query
    -> fields('hec')
    -> condition('hec.he_id', $he_id, '=')
    -> orderBy('he_content_id', 'ASC')
    -> orderBy('he_updated_date', 'ASC')
    -> execute()
    -> fetchAll();
  }
  catch (Exception $e) {
    return(FALSE);
  }

  // do we have any content? parsing succesful?
  if(count($actual_content)){
    // clean the content from empty and illegal chars first
    foreach($actual_content as $key => $value){
      if(clean_tag($value['he_content'])){
        $filtered_actual_content[] = array(
            'he_id' => $value['he_id'],
            'he_tag' => $value['he_tag'],
            'he_content' => str_replace(array('<br/>', '<br />'), "\n", trim($value['he_content'])),
            'he_updated_date' => time(),
        );
      }
    }
  }

  // in case of previous content existance
  if(count($query_result) > 0){
    $i = 0;

    while($i < count($filtered_actual_content) && !$the_flag){
      if($query_result[$i]->he_content != $filtered_actual_content[$i]['he_content']){
        // True flag = do the update
        $the_flag = TRUE;
      }
      $i++;
    }
  }
  // by default do the update
  else{ $the_flag = TRUE; }
  // if we have previous content or the content was changed, remove old content
  if (count($query_result) > 0 && $the_flag && $he_update_flag == 1) {
    dpr(count($he_update_flag));
    try {
      $delete_result = db_delete('html_extract_content') -> condition('he_id', $he_id) ->execute();
   
    }
    catch (Exception $e) {
      drupal_set_message(t('DB deletion failed. Message = %message, query= %query',
          array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    }
  }
  // if we have new content or if content was changed
  if (count($filtered_actual_content) > 0 && $the_flag && $he_update_flag < 2) {
    try {
      // add the new values to html_extract_content
      $query_content = db_insert('html_extract_content')->fields(array('he_id', 'he_tag', 'he_content', 'he_updated_date'));
      foreach ($filtered_actual_content as $row) {
        $query_content->values($row);
      }
      $query_content -> useDefaults(array('he_content_id'))
             ->execute();
    }
    catch (Exception $e) {
      drupal_set_message(t('DB Insertion failed. Message = %message, query= %query',
          array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    }
  }
  // on merge old and new content
  elseif(count($filtered_actual_content) > 0 && $the_flag && $he_update_flag == 2){

    try {
      // remove existent content
      $existent_query  = db_delete('html_extract_content');
      $ored = db_or();

      foreach($filtered_actual_content as $key => $value){
            $ored -> condition('he_content',  $value['he_content'], '=');
      }

      $existent_query -> condition($ored)
                      -> condition('he_id', $he_id, '=')
                      -> execute();

      // insert new values
      $query_content = db_insert('html_extract_content')->fields(array('he_id', 'he_tag', 'he_content', 'he_updated_date'));
      foreach ($filtered_actual_content as $row) {
        $query_content->values($row);
      }
      $query_content -> useDefaults(array('he_content_id'))
             ->execute();

    }
    catch (Exception $e) {
      watchdog( 'HTML Extract','Failure at HTML extract selection: <pre> %e </pre>', array('%e' =>  $e->getMessage()), WATCHDOG_ERROR);
    }
  }
  
  try {
    // update the html_extract table and actualize update date even 
    // if content wasn't updated - last update date should be actual
    $query_main = db_update('html_extract')
    ->fields(array(
        'he_last_update' => mktime(24, 0, 0, date("n"), date("j"), date("Y")),
    ))
    ->condition('he_id', $he_id, '=')
    ->execute();
  }
  catch (Exception $e) {
    drupal_set_message(t('Main table update failed. Message = %message, query= %query',
        array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
  }
  
  return($the_flag);
}

function update_html_extract_data(){
  $he_ids = NULL;
  // default return array values
  $ret_msg_arr = array(
      'he_id' => '',
      'he_cron' => '',
      'he_url' => '',
      'he_parse_tag' => '',
      'he_update_flag' => '',
  );

  $today_midnight = mktime(24, 0, 0, date("n"), date("j"), date("Y"));
  $he_schedule = array(t('never'), t('each run'), t('daily'), t('weekly'));
  
  try {
    $query = db_select('html_extract', 'he');
    $query_result = $query
    -> fields('he')
    -> condition('he.he_cron', 0, '!=')
    -> orderBy('he_last_update ', 'DESC')
    -> execute()
    -> fetchAll();
  }
  catch (Exception $e) {
    drupal_set_message(t('Selection failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
  }

  foreach ($query_result as $key => $value) {
    if ( $value->he_cron == 1 || 
        $value->he_cron == 2 && $value->he_last_update + 86399 < $today_midnight ||
        $value->he_cron == 3 && $value->he_last_update + 604799 < $today_midnight) {
      $temp_content = extract_content($value->he_url, $value->he_parse_tag, $value->he_id);
      // if update was made build a list with IDs
      if($temp_content && update_html_content($temp_content, $value->he_id, $value->he_update_flag)){
        $he_ids .= $value->he_id . '_';
      }

      $ret_msg_arr['he_id'] .= $value->he_id . ', ';
      $ret_msg_arr['he_cron'] .= $he_schedule[$value->he_cron] . ', ';
      $ret_msg_arr['he_url'] .= $value->he_url . ', ';
      $ret_msg_arr['he_parse_tag'] .= $value->he_parse_tag . ', ';
      $ret_msg_arr['he_update_flag'] .= $value-> he_update_flag . ', ';
    }
  }

  if(!empty($he_ids)){
    // notify rules action
    rules_invoke_event('html_extract_entity_update', $he_ids);
    watchdog('HTML Extract', 'Updated content: %he_id cron: %he_cron from url: %he_url tag: %he_parse_tag - %he_update_flag', array(
      '%he_id' => $ret_msg_arr['he_id'], 
      '%he_cron' => $ret_msg_arr['he_cron'],
      '%he_url' => $ret_msg_arr['he_url'],
      '%he_parse_tag' => $ret_msg_arr['he_parse_tag'],
      '%he_update_flag' => $ret_msg_arr['he_update_flag']
      ));
  }
  return(TRUE);
}

function flush_html_extract_data($html_export){

  try {
    db_delete('html_extract_content') -> condition('he_id', $html_export->he_id) ->execute();
  }
  catch (Exception $e) {
    drupal_set_message(t('DB deletion failed. Message = %message, query= %query',
        array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
  }
  
  drupal_set_message(t('Flushed: ') . $html_export->he_id . ' - ' . $html_export->he_url);
  drupal_goto('admin/config/html_extract/overview');
  return(TRUE);
}
