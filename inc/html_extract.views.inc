<?php

/**
 * @file
 * Provides the QR code fields for views
 * Defines the default view for QR codes
 */

/**
 * Implements hook_views_data().
 *
 */


function html_extract_views_data() {

  /* default fields start */
  $plain_text = array(
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $numeric_field = array(
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $timestamp_field = array(
    'sort' => array(
        'handler' => 'views_handler_sort_date',
    ),
    'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_date',
    ),
  );
  /* default fields end */

//  html_extract table
  $data['html_extract']['table']['group'] = t('HTML Extract');

  $data['html_extract']['table']['base'] = array(
    'field' => 'he_id',
    'title' => t('HTML Extract'),
    'help' => t('HTML Extract setting table.')
  );

  $data['html_extract']['he_id'] = array(
    'title' => t('ID'),
    'help' => t('Setting ID.'),
  );
  $data['html_extract']['he_id'] += $numeric_field;

  $data['html_extract']['he_url'] = array(
    'title' => t('Page URL setting'),
    'help' => t('The HTML Extract page URL.'),
  );
  $data['html_extract']['he_url'] += $plain_text;
  
  $data['html_extract']['he_parse_tag'] = array(
      'title' => t('Parse tag setting'),
      'help' => t('The HTML Extract page parse tag.'),
  );
  $data['html_extract']['he_parse_tag'] += $plain_text;
  
  $data['html_extract']['he_cron'] = array(
      'title' => t('CRON setting'),
      'help' => t('HTML Extrat CRON Schedule.'),
  );
  $data['html_extract']['he_cron'] += $plain_text;

  $data['html_extract']['he_last_update'] = array(
      'title' => t('Last update'),
      'help' => t('HTML Extrat Update point.'),
  );
  $data['html_extract']['he_last_update'] += $timestamp_field;

  // html_extract_content table
  $data['html_extract_content']['table']['group'] = t('HTML Extract');
  
  $data['html_extract_content']['table']['base'] = array(
      'field' => 'he_content_id',
      'title' => t('HTML Extract Content'),
      'help' => t('HTML Extract content table.')
  );
  
  $data['html_extract_content']['table']['join'] = array(
      'html_extract' => array(
          'left_field' => 'he_id',
          'field' => 'he_id',
      ),
  );
  
  $data['html_extract_content']['he_content_id'] = array(
      'title' => t('Content row ID'),
      'help' => t('The HTML Extract row id.'),
  );
  $data['html_extract_content']['he_content_id'] += $numeric_field;  

  $data['html_extract_content']['he_id'] = array(
      'title' => t('Setting reference ID'),
      'help' => t('The HTML Extract Content - reference ID.'),
  );
  $data['html_extract_content']['he_id'] += $numeric_field;

  $data['html_extract_content']['he_tag'] = array(
      'title' => t('Content tag'),
      'help' => t('The HTML Extract Content tag.'),
  );
  $data['html_extract_content']['he_tag'] += $plain_text;

  $data['html_extract_content']['he_content'] = array(
      'title' => t('Content Text'),
      'help' => t('The HTML Extract content Text.'),
  );
  $data['html_extract_content']['he_content'] += $plain_text;
  
  $data['html_extract_content']['he_updated_date'] = array(
      'title' => t('Update date'),
      'help' => t('The HTML Extract content update.'),
  );
  $data['html_extract_content']['he_updated_date'] += $timestamp_field;

  return $data;
}

/**
 * Implements hook_views_post_render().
 */

function html_extract_views_pre_render(&$view){

  if($view->base_table == 'html_extract' && isset($view->result[0]->html_extract_he_cron)){
    foreach($view->result as $key => $value){
      $view->result[$key]->html_extract_he_cron = cron_formatter($value->html_extract_he_cron);
    }
  }
}

function html_extract_views_post_render(&$view, &$output, &$cache){
  if($view->base_table == 'html_extract'){
    $output =  html_entity_decode($output);
  }
}

/**
 * Implements hook_views_default_views().
 */

function html_extract_views_default_views() {

  $view = new view;
  $view->name = 'html_extract_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'html_extract';
  $view->human_name = 'HTML Extract List';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'HTML Extract List';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
      'counter' => 'counter',
      'he_id' => 'he_id',
      'he_parse_tag' => 'he_parse_tag',
      'he_url' => 'he_url',
      'he_last_update' => 'he_last_update',
  );
  $handler->display->display_options['style_options']['default'] = 'he_last_update';
  $handler->display->display_options['style_options']['info'] = array(
      'counter' => array(
          'align' => '',
          'separator' => '',
      ),
      'he_id' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
      ),
      'he_parse_tag' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
      ),
      'he_url' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
      ),
      'he_last_update' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
      ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="/html_extract/add">Add new HTML Extraction</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['empty'] = FALSE;
  $handler->display->display_options['footer']['area']['content'] = '* The content will be actualized regarding CRON setting at least on next CRON run.';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  $handler->display->display_options['footer']['area']['tokenize'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No HTML Extract content';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No HTML Extract content could be found. Use the Add new content links to add new HTML Extract content.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['external'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['html'] = 0;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['counter']['hide_empty'] = 0;
  $handler->display->display_options['fields']['counter']['empty_zero'] = 0;
  $handler->display->display_options['fields']['counter']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: HTML Extract: ID */
  $handler->display->display_options['fields']['he_id']['id'] = 'he_id';
  $handler->display->display_options['fields']['he_id']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_id']['field'] = 'he_id';
  /* Field: HTML Extract: Parse tag setting */
  $handler->display->display_options['fields']['he_parse_tag']['id'] = 'he_parse_tag';
  $handler->display->display_options['fields']['he_parse_tag']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_parse_tag']['field'] = 'he_parse_tag';
  $handler->display->display_options['fields']['he_parse_tag']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_parse_tag']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_parse_tag']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_parse_tag']['hide_alter_empty'] = 0;
  /* Field: HTML Extract: Page URL setting */
  $handler->display->display_options['fields']['he_url']['id'] = 'he_url';
  $handler->display->display_options['fields']['he_url']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_url']['field'] = 'he_url';
  $handler->display->display_options['fields']['he_url']['label'] = 'Source URL';
  $handler->display->display_options['fields']['he_url']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_url']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_url']['hide_alter_empty'] = 0;
  /* Field: HTML Extract: Last update */
  $handler->display->display_options['fields']['he_last_update']['id'] = 'he_last_update';
  $handler->display->display_options['fields']['he_last_update']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_last_update']['field'] = 'he_last_update';
  $handler->display->display_options['fields']['he_last_update']['label'] = 'Last updated';
  $handler->display->display_options['fields']['he_last_update']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_last_update']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_last_update']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_last_update']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_last_update']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_last_update']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_last_update']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_last_update']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_last_update']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['he_last_update']['date_format'] = 'short';
  /* Field: HTML Extract: CRON setting */
  $handler->display->display_options['fields']['he_cron']['id'] = 'he_cron';
  $handler->display->display_options['fields']['he_cron']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_cron']['field'] = 'he_cron';
  $handler->display->display_options['fields']['he_cron']['label'] = 'CRON Update';
  $handler->display->display_options['fields']['he_cron']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_cron']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_cron']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_cron']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_cron']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_cron']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_cron']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_cron']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_cron']['hide_alter_empty'] = 0;
  /* Field: HTML Extract: ID */
  $handler->display->display_options['fields']['he_id_1']['id'] = 'he_id_1';
  $handler->display->display_options['fields']['he_id_1']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_id_1']['field'] = 'he_id';
  $handler->display->display_options['fields']['he_id_1']['label'] = 'Edit';
  $handler->display->display_options['fields']['he_id_1']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['he_id_1']['alter']['text'] = 'edit';
  $handler->display->display_options['fields']['he_id_1']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['he_id_1']['alter']['path'] = 'html_extract/[he_id]/edit';
  $handler->display->display_options['fields']['he_id_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_id_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_id_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_id_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_id_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_id_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_id_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_id_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_id_1']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['he_id_1']['format_plural'] = 0;
  /* Field: HTML Extract: ID */
  $handler->display->display_options['fields']['he_id_2']['id'] = 'he_id_2';
  $handler->display->display_options['fields']['he_id_2']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_id_2']['field'] = 'he_id';
  $handler->display->display_options['fields']['he_id_2']['label'] = 'Delete';
  $handler->display->display_options['fields']['he_id_2']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['he_id_2']['alter']['text'] = 'remove';
  $handler->display->display_options['fields']['he_id_2']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['he_id_2']['alter']['path'] = 'html_extract/[he_id]/remove';
  $handler->display->display_options['fields']['he_id_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_id_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_id_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_id_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_id_2']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_id_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_id_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_id_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_id_2']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['he_id_2']['format_plural'] = 0;
  /* Field: HTML Extract: ID */
  $handler->display->display_options['fields']['he_id_3']['id'] = 'he_id_3';
  $handler->display->display_options['fields']['he_id_3']['table'] = 'html_extract';
  $handler->display->display_options['fields']['he_id_3']['field'] = 'he_id';
  $handler->display->display_options['fields']['he_id_3']['label'] = 'Flush content';
  $handler->display->display_options['fields']['he_id_3']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['he_id_3']['alter']['text'] = 'flush';
  $handler->display->display_options['fields']['he_id_3']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['he_id_3']['alter']['path'] = 'html_extract/[he_id]/flush';
  $handler->display->display_options['fields']['he_id_3']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['external'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['he_id_3']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['he_id_3']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['he_id_3']['alter']['html'] = 0;
  $handler->display->display_options['fields']['he_id_3']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['he_id_3']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['he_id_3']['hide_empty'] = 0;
  $handler->display->display_options['fields']['he_id_3']['empty_zero'] = 0;
  $handler->display->display_options['fields']['he_id_3']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['he_id_3']['format_plural'] = 0;
  $translatables['html_extract_list'] = array(
      t('Master'),
      t('HTML Extract List'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('<a href="/html_extract/add">Add new HTML Extraction</a>'),
      t('* The content will be actualized regarding CRON setting at least on next CRON run.'),
      t('No HTML Extract content'),
      t('No HTML Extract content could be found. Use the Add new content links to add new HTML Extract content.'),
      t('#'),
      t('ID'),
      t('.'),
      t(','),
      t('Parse tag setting'),
      t('Source URL'),
      t('Last updated'),
      t('CRON Update'),
      t('Edit'),
      t('edit'),
      t('html_extract/[he_id]/edit'),
      t('Delete'),
      t('remove'),
      t('html_extract/[he_id]/remove'),
      t('Flush content'),
      t('flush'),
      t('html_extract/[he_id]/flush'),
  );

  $views[$view->name] = $view;

  return $views;
}


